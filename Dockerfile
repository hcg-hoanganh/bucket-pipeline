# specify a base image
FROM node:alpine

WORKDIR /usr/app
# install some Dependencies and set caching
COPY ./package*.json ./
RUN npm install
COPY ./ ./

# Default commands

CMD ["npm" , "start"]

# some script useful:
# docker build -t ngha/webapi:latest .
# docker run -p 8089:8080 --name container-webapi ngha/webapi
# docker run -it ngha/webapi sh
# docker exec -it container-webapi sh
