#Docker script


Docker command:

# Run a image to create a container
docker run –-name <name_container> -v <folder_on_desktop>:<folder_on_container> -p<port_desktop>:<port_container> <image name> bash
Example: docker run –name eva_nginx -p 80:80 -d nginx
* docker run : command run of docker
* --name: create a name of container eva_nginx . Name must to unique, if don't config name property docker would generate.
* -p: Publish a container’s port(s) to the host IP public 192.168.99.100
* -d: Run container in background and print container ID
* nginx: The name of image

# Show list container
docker ps -a  # show all container
docker ps # show only containers are running background

# Run a command in a running container
docker exec -it <container_id or name_container> bash

# Newer versions of Docker now have the system prune command.

### To remove dangling images:

```$ docker system prune```

### To remove dangling as well as unused images:

```$ docker system prune --all```

### To prune volumes:

```$ docker system prune --volumes```

### To prune the universe:

```$ docker system prune --force --all --volumes```

---
# Example: Create jenkins-server

### pull image jenkins
```docker pull jenkins/jenkins```


### create container
```docker run --name jenkins-server -d -v jenkins_home:/var/jenkins_home -p 8080:8080 -p 50000:50000 jenkins/jenkins:latest ```

### jenkins login first
```docker exec -ti jenkins-server /bin/bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"```

create admin user account: admin - admin

### install more plugins:
- docker
- docker API
- docker pipeline

# connect Jenkins ssh github
## create ssh key
ssh-keygen

cat var/jenkins_home/.ssh/id_rsa.pub

## jenkins add credential
cat var/jenkins_home/.ssh/id_rsa

## jenkins connect github repository 
git@github.com:hoanganh-nguyen94/nx-mono-workspace-root.git

## github generate token 
6e09322006b5b4655f990860f08b63b260a94fe8

## create Job item 
> name
 nx-workspace-application-admin 
pipeline
- check: github project
- check: GitHub hook trigger for GITScm polling

## configure system


 
